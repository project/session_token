## Summary
This module allows you to retrieve data from the $_SESSION storage and use that
information inside any form which accepts tokens.

A good example can be the usage of a form which you want to populate with
data which has already been entered by the current (anonymous) user.

Another example is to populate a multistep form.

Developers can easily store and retrieve information from this storage using the
functions `session_token_get($key)` and `session_token_set($key, $value)`.

## REQUIREMENTS

This module requires the token module.

## INSTALLATION

* Install as usual, see http://drupal.org/node/895232 for further information.

## CONFIGURATION

This module has no configuration.

## CUSTOMIZATION

No customization is possible.

## TROUBLESHOOTING

No issues detected so far.

## FAQ

No questions have been asked so far.

## CONTACT

Current maintainers:
* Levi Govaerts (legovaer) - https://www.drupal.org/u/legovaer
